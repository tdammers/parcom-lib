module Main where

import Text.Parcom.Core
import Text.Parcom.Prim
import Text.Parcom.Combinators
import Text.Parcom.ByteString
import Text.Parcom.Textual
import qualified Data.Map as Map
import Data.Map (Map)
import Data.Char (isSpace, isDigit, isHexDigit, ord, chr)
import Control.Monad
import Control.Monad.Trans.Class (lift)
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.Word8 (Word8)
import qualified Data.ByteString.Char8 as BS8
import Data.ByteString.Char8 ()
import Data.Maybe (isJust)

chr8 :: Word8 -> Char
chr8 = chr . fromIntegral
ord8 :: Char -> Word8
ord8 = fromIntegral . ord

data Value = NullV
           | IntV Integer
           | StringV String
           | ArrayV [ Value ]
           | HashV (Map String Value)
           deriving (Show, Eq)

skipSpaces = skip $ many $ satisfy (isSpace . chr8)

valueP = intP <|> stringP <|> arrayP <|> hashP <|> nullP <?> "JSON value"

nullP = do
    string "null"
    return NullV

intP = do
    negative <- isJust `liftM` possibly (char '-')
    str <- many1 digitP
    let i = read $ map chr8 $ str
    return $ IntV $ if negative then -i else i

digitP = satisfy $ isDigit . chr8

hexDigitP = satisfy $ isHexDigit . chr8

stringP = do
    char '"'
    str <- many stringCharP
    char '"'
    return $ StringV $ str

stringCharP = simpleCharP
            <|> escapedCharP
            <?> "character"

simpleCharP =
    chr8 `liftM` satisfy p
    where
        p c =
            c /= ord8 '"' &&
            c < 128 &&
            c >= 32 &&
            c /= ord8 '\\'

escapedCharP = do
    char '\\'
    c <- chr8 `liftM` anyToken
    case c of
        'n' -> return '\n'
        't' -> return '\t'
        'r' -> return '\r'
        'b' -> return '\b'
        'v' -> return '\v'
        '0' -> return '\0'
        'u' -> do
            ns <- map chr8 `liftM` times 4 hexDigitP
            let n = read ("0x" ++ ns)
            return $ chr n
        _ -> return c


arrayP = do
    char '['
    skipSpaces
    items <- manySepBy (valueP `before` skipSpaces) (char ',' `before` skipSpaces)
    char ']'
    return $ ArrayV items

hashP = do
    char '{'
    skipSpaces
    items <- manySepBy (hashPairP `before` skipSpaces) (char ',' `before` skipSpaces)
    char '}'
    return $ HashV $ Map.fromList items
    where
        hashPairP = do
            StringV key <- stringP
            skipSpaces
            char ':'
            skipSpaces
            value <- valueP
            return (key, value)

formatParcomError e =
    let sp = peSourcePosition e
    in concat
        [ "*** error: "
        , peErrorDescription e
        , " in "
        , posFileName sp
        , ":"
        , show $ posLine sp
        , ":"
        , show $ posColumn sp
        ]

main = do
    putStrLn "Enter some JSON plz mang"
    ln <- (BS.pack . map ord8) `liftM` getLine
    pr <- parseT (valueP `before` eof) "<STDIN>" ln
    case pr of
        Left err -> putStrLn $ formatParcomError err
        Right x -> print x
    main
