{-#LANGUAGE MultiParamTypeClasses #-}
module Text.Parcom.ByteString.Strict
where

import Prelude hiding (head, null, tail)
import Data.ByteString
import Data.Word8
import Text.Parcom.Stream
import Text.Parcom.Word8
import Data.ByteString.UTF8 as UTF8

instance Stream ByteString Word8 where
    peek = head
    atEnd = null
    consume = tail

instance Listish ByteString Word8 where
    toList = unpack
    fromList = pack

instance Textish ByteString where
    peekChar s =
        case UTF8.decode s of
            Just ('\65533', _) -> (Nothing, 0)
            Nothing -> (Nothing, 0)
            Just (c, n) -> (Just c, n)
