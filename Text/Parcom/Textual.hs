module Text.Parcom.Textual
where

import Data.String
import Text.Parcom.Prim
import Text.Parcom.Core
import Text.Parcom.Internal
import Text.Parcom.Stream (Stream, Listish)

string :: (Monad m, Stream s t, Listish s t, IsString s, Eq t, Show t, Token t) => String -> ParcomT s t m String
string str = do
    prefix $ fromString str
    return str

char :: (Monad m, Stream s t, Listish s t, IsString s, Eq t, Show t, Token t) => Char -> ParcomT s t m Char
char c = string [c] >> return c
