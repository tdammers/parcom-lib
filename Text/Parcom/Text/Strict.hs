{-#LANGUAGE MultiParamTypeClasses, OverloadedStrings #-}
module Text.Parcom.Text.Strict
where

import Prelude hiding (head, null, tail)
import Data.Text
import Text.Parcom.Stream

instance Stream Text Char where
    peek = head
    atEnd = null
    consume = tail

instance Listish Text Char where
    toList = unpack
    fromList = pack

instance Textish Text where
    peekChar "" = (Nothing, 0)
    peekChar s = (Just (head s), 1)
