module Text.Parcom.Word8
(
)
where

import Text.Parcom.Stream
import Data.Word8

instance Token Word8 where
    isLineDelimiter = (== 13)
