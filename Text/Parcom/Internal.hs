module Text.Parcom.Internal
where

formatOptionList :: [String] -> String
formatOptionList (x:y:[]) = x ++ " or " ++ y
formatOptionList (x:[]) = x
formatOptionList [] = ""
formatOptionList (x:xs) = x ++ ", " ++ formatOptionList xs
